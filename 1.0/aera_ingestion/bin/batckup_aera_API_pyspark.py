#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

#os.environ["PYSPARK_PYTHON"] = "/data2/anaconda2/bin/python"
#os.environ["PYSPARK_DRIVER_PYTHON"] = "/data2/anaconda2/bin/python"
#os.environ["PYSPARK_SUBMIT_ARGS"] = "--packages com.databricks:spark-csv_2.11:1.5.0 pyspark-shell"
#os.environ["PYSPARK_SUBMIT_ARGS"] = "--jars /usr/hdp/current/spark-client/lib/spark-csv_2.10-1.5.0.jar,/usr/hdp/current/spark-client/lib/commons-csv-1.5.jar pyspark-shell"

sys.path.insert(0, '/usr/hdp/current/spark-client/python/')
sys.path.insert(0, '/usr/hdp/current/spark-client/python/lib/py4j-0.9-src.zip')

import requests
import codecs
#import FusionOpsConfig  as foc
import json
import datetime as dt
import sys
#------pyspark
import pandas as pd
#import numpy as np
import json
from pyspark import SparkContext, SparkConf, SQLContext
from pyspark.sql import HiveContext
import pyspark
#import ConfigParser
import os
import subprocess
#import commands
import time
from py4j.protocol import Py4JJavaError
#$SPARK_HOME/bin/spark-shell --packages com.databricks:spark-csv_2.11:1.5.0
#TODO: Needs Exception handling code

global host;
global protocol;
global clientId;
global clientSecret;
global loginId;
global password;
global oAuthTokenURL;
global reportURL;
global protocol_sep;
#global no_rows;

conf =SparkConf().setAppName('pyspark').setMaster('yarn-cluster')
sc =SparkContext(conf=conf)
sqlContext = SQLContext(sc)
hive_context = HiveContext(sc)

keytab_file=sys.argv[1]
print("Keytab file")
print(keytab_file)
os.system("kinit -kt %s s114041@EU.MERCKGROUP.COM"%keytab_file)

sprop = {}
tmp=[]

print("Using pyspark to fetch config parameters")
#log_date = subprocess.check_output("date +%d-%m-%Y", shell=True).strip()
#print "log_date:",log_date
#log_file = '/tmp/'+"Supplier_to_customer_" + log_date + ".log"
#print "log_file:",log_file

df = sc.textFile("/user/s114041/IMS_VPD/aera_ingest/bin/aera_config.config") #--------Config file path from HDFS containing details of API change accordingly 
#print(df)
for line in df.collect():
#    print(line)
    for x in line.split(','):
	     key=x.split(':')[0]
	     value=x.split(':')[1]
	     print(key,value)
	     tmp.append(value)
	     sprop[key.encode('ascii')]=value.encode('ascii')
             
    
print(tmp)
host, protocol, clientId, clientSecret, loginId, password, oAuthTokenURL, reportURL = [tmp[i] for i in (0, 1, 2, 3, 4, 5, 6, 7)]
print("host: ",host)
print("protocol: ",protocol)
print("clientId: ",clientId)
print("clientSecret: ",clientSecret)
print("loginId: ",loginId)
print("password: ",password)
print("oAuthTokenURL: ",oAuthTokenURL)
print("reportURL: ",reportURL)
protocol_sep='://'
print(host,protocol,clientId,clientSecret,loginId,password,oAuthTokenURL,reportURL,protocol_sep)
print("Fetching of Config parameters successfull")
	
# proxy setting - should come from config setting

PROXIES = {'https': "http://ssvc-wsus_srv:SecureServer123@proxy.eu.merckgroup.com:8080",
           'http': "http://ssvc-wsus_srv:SecureServer123@proxy.eu.merckgroup.com:8080"}


# FusionOps Error Codes
# FO_ERROR_1001 = 'FO_ERROR_1001'
# FO_ERROR_1002 = 'FO_ERROR_1002'
# FO_ERROR_1003 = 'FO_ERROR_1003'
# FO_ERROR_1004 = 'FO_ERROR_1004'
# FO_ERROR_1005 = 'FO_ERROR_1005'
# FO_ERROR_1008 = 'FO_ERROR_1008'


FO_API_TOKEN_QRY_PARAM = '?accessToken={}'      # Used for listing all reports user has access to
FO_API_URL_RPTS_GET = '/{}?accessToken={}&rowStart={}&pageSize={}' #
FO_API_URL_RPTS_MTD = '/{}/metadata?accessToken={}'

FO_API_OPTS = {'Content-Type': 'application/x-www-form-urlencoded',
               'Accept-Charset': 'UTF-8',
               'Cache-Control': 'no-cache',
               'grant_type': 'password',
               'client_id': None,
               'client_secret': None,
               'username': None,
               'password': None}

FO_API_OPTS_REFRESH = {'Content-Type': 'application/x-www-form-urlencoded',
                       'Accept-Charset': 'UTF-8',
                       'Cache-Control': 'no-cache',
                       'grant_type': 'refresh_token',
                       'client_id': None,
                       'client_secret': None,
                       'refresh_token' : None }

# ERROR CHECKING
ERROR_CHECK_OK = 0
ERROR_CHECK_FAIL = -1
ERROR_CHECK_TOKEN_EXPIRE = 1

def error_check(responseObject): 
    """
    Helper Function for validation of the results from the API call
    :param responseObject: result from request.get call
    :return:
    """
    if responseObject != 200:
        details = responseObject.json
        msg = details['message']
        fops_error = details['error']
        # if fops_error == FOPS_ERROR_
        raise Exception(msg)
class ApiEngine(object):
    """
    For calling FusionOps Web Api Reports - this is a Generator Object.
    Used with ApiToken and ApiReportUrl objects.
    These objects encapsulate the token refresh and url substitution of
    token, report name and pagestart values in the urls for FusionOps API calls

    Usage:
    api_token = ApiToken(config_object, PROXIES)
    api_report = ApiReportUrl(<report_name>, report_url_base)
    api_engine = ApiEngine(api_token, api_report)

    for data in api_engine:
        ...do something with data (batch of records)


    """
   
    def __init__(self, api_token_object, api_report_url_object):
        self.report_url = api_report_url_object
        self.api_token = api_token_object
        self.fields = None
        self._opts_data = {'Cache-Control': 'no-cache'}

    @staticmethod
    def has_data(response_data):
        return response_data.__contains__('data')

    @staticmethod
    def has_error(response_data):
        return response_data.__contains__('errorCode')

    def __iter__(self):
        data = None
        row_start = self.report_url.row_start
        print('row_start'+str(row_start))
        print('row_end'+str(self.report_url.row_end))
        while True:
            data=self._call_api()
            if self.has_error(data):
                print(data['message'])
                break
            if not self.has_data(data):
                self.api_token.refresh()
                self.report_url.token_string = self.api_token.token
                data = self._call_api()
		#print("printing data")
		
		#print(type(data))
            if not self.fields:
                self.fields = data['metaData']['fields']
                #print self.fields
                #sys.exit()
            yield self.fields, data['data']

            # ...verify if we're at the end of the data set
            meta_data = data['metaData']
            row_count = meta_data['totalRows']
            print('total rows: ' + str(row_count))
            print('page_size: ' + str(self.report_url.page_size))
            if row_count < self.report_url.page_size:
                break
            #if row_start % 10000 == 0:
            #     print row_start
            row_start += self.report_url.page_size
            # ...stop if we pulled all the records (Note: need to be abe to pull exact number of records)
            if self.report_url.row_end:
                if row_start > self.report_url.row_end:
                    raise StopIteration

            self.report_url.next_page()

    def _get_data(self):
        data = self._call_api()
        if not self.has_data(data):
            self.api_token.refresh()
            self.report_url.token_string = self.api_token.token
            data = self._call_api()
	#print(key,value)
        return data


    def _call_api(self):
        self._init_url_token() # need to inject token into url
        the_data = self._send_request(self.report_url.url, self._opts_data, proxies=PROXIES)
        return the_data

    def _init_url_token(self):
        if self.report_url.token_string is None:
            self.report_url.token_string = self.api_token.token

    def set_report_object(self, api_url_object):
        self.report_url = api_url_object

    def get_report_metadata(self):
        self._init_url_token()  # need to inject token into url
        the_data = self._send_request(self.report_url.url_metadata, self._opts_data, proxies=PROXIES)
        return the_data

    def get_reports_list(self):
        self._init_url_token()  # need to inject token into url
        the_data = self._send_request(self.report_url.url_all_reports, self._opts_data, proxies=PROXIES)
        return the_data

    def _send_request(self, url, options, proxies=None):
        results = requests.get(url, options, proxies=proxies)
        # status = error_check(results)
        the_data = results.json()
        return the_data
	

class ApiToken(object):
    """
        Represents a Fusionops REST API Token.
        Notes:
        1. Uses lazy evaluation.  The token is not fetched until the
        'token' property is called for the first time.
        2. The 'owner' of this object is responsible for refreshing (default is 60 seconds)
        3. Refersh uses the refresh token, provided when the token is first requested and
        uses the 'grant_type': 'refresh_token'
    """
    def __init__(self,config_object,proxies=None):
        self.options = FO_API_OPTS
        self.options_refresh = FO_API_OPTS_REFRESH

        self.options['client_id'] = clientId
        self.options['client_secret'] = clientSecret
        self.options['username'] = loginId
        self.options['password'] = password

        self.options_refresh['client_id'] = clientId
        self.options_refresh['client_secret'] = clientSecret

        self.oAuth_url =config_object.oAuth_token

        self._access_token = None
        self._refresh_token = None
        self._proxies = proxies
	#print("Inside api token")
    @property
    def token(self):
        if not self._access_token:
            self._renew_token(self.options)
        return self._access_token

    def _get_json_response(self, options):
        request_response = requests.post(self.oAuth_url, options, proxies=self._proxies)
        result=request_response.json()
        return result

    def _renew_token(self, options):
        json_response = self._get_json_response(options)
        self._update(json_response)

    def refresh(self):
        self._renew_token(self.options_refresh)
        return self.token

    def _update(self, json_msg):
        self._access_token = json_msg['access_token']
        self._refresh_token = json_msg['refresh_token']
        self.options_refresh['refresh_token'] = self._refresh_token


class ApiReportUrl(object):
    """
    Encapsulates the FusionOps Report URL by inserting the token and row start values, etc
    Example:
    'https://insightuk.fusionopscloud.com/ispring/client/v1/reports/<REPORT_NAME>?accessToken=XXXX&rowstart=0&pageSize=1000'
    and the report meta data:
    'https://insightuk.fusionopscloud.com/ispring/client/v1/reports/<REPORT_NAME>/metadata?accessToken=XXXX'

    """
    def __init__(self, report_name, reports_url, page_size=1000, row_start=0, row_end=None):
        """
        :param report_name: actual FusioOps apiURL report Name
        :param report_url: the base url eg https://insightuk.fusionopscloud.com/ispring/client/v1/reports
        :param page_size: the number of record per page - see FusionOps documentation
        :param page_size: the number of record per page - see FusionOps documentation
        :param row_start: the record number to start the fetch
        :param row_end: the last record you want
        """
        self._row_start = row_start
        self._page_size = page_size
        self._row_end = row_end
        self._token = None
        self.current_url = None
        self.curr_url_meta = None
        self.all_reports_url = None
        self.all_reports_url_templ = reports_url + FO_API_TOKEN_QRY_PARAM
        self.url_template = reports_url + FO_API_URL_RPTS_GET.format(report_name, '{}', '{}', self.page_size)
        self.url_templ_meta = reports_url + FO_API_URL_RPTS_MTD.format(report_name, '{}')

    def _set_urls(self):
        self.current_url = self.url_template.format(self._token, self.row_start)
        self.curr_url_meta = self.url_templ_meta.format(self._token)
        self.all_reports_url = self.all_reports_url_templ.format(self._token)

    @property
    def row_end(self):
        return self._row_end

    @property
    def row_start(self):
        return self._row_start

    @property
    def page_size(self):
        return self._page_size

    @property
    def token_string(self):
        return self._token

    @token_string.setter
    def token_string(self, token):
        self._token = token
        self._set_urls()

    def next_page(self):
        self._row_start += self._page_size
        self._set_urls()

    @property
    def url(self):
        if not self._token:
            print("can't call url without a token")
        return self.current_url

    @property
    def url_metadata(self):
        if not self._token:
            print("can't call url_metadata without a token")
        return self.curr_url_meta

    @property
    def url_all_reports(self):
        if not self._token:
            print("can't call url_all_reports without a token")
        return self.all_reports_url

"""
FusionOps Configuration Object.

This sets up the parameters using the global variables from pyspark 
"""

class FusionOpsConfig(object):
    print(protocol)
    print(protocol_sep)
    print(host)
    print(oAuthTokenURL)
    oAuth_token=str(protocol)+(protocol_sep)+host+oAuthTokenURL
    report_url_base = protocol+protocol_sep+host+reportURL
    user_id = loginId
    user_password = password
    client_id = clientId
    client_secret =clientSecret		
    print("++++Parameters from FusionOpsConfig++++")
    print(oAuth_token,report_url_base,user_id,user_password,client_id,client_secret)

def run_report(api_report_name,output_file,column_names = False):
   
    print(api_report_name)
	 
    config_object=FusionOpsConfig()
    fo_api_token = ApiToken(config_object,PROXIES)
    fo_api_rpt_url = ApiReportUrl(api_report_name,config_object.report_url_base,10000)
    fo_api_engine=ApiEngine(fo_api_token,fo_api_rpt_url)
    
    #bom_backup(api_report_name)
    
    no_rows=0
    run_date=dt.datetime.today().strftime('%d-%m-%Y')
    run_year=dt.datetime.today().strftime('%Y')
    run_month=dt.datetime.today().strftime('%b')
    #reports = fo_api_engine.get_reports_list() 
    #print(reports)
    
    
    for fields,data in fo_api_engine:
        no_rows += len(data)
        print("Row Count="+str(no_rows))
        #no_col = len(fields)   
        data_df = pd.DataFrame.from_dict(data)
        if len(data_df) != 0:
            spark_df = hive_context.createDataFrame(data_df)
            #spark_df.show()
            spark_df.write.format('com.databricks.spark.csv').options(header = 'true', delimiter = "|").mode('overwrite').save(output_file+run_year+"/"+run_month+"/"+api_report_name+"/"+str(no_rows))
	   #spark_df.write.save(path='output_file+api_report_name',format='csv',mode='append',sep="|")
	    
	   #df.write.save(path='csv', format='csv', mode='append', sep='\t')    
        else:
    	    print("INFO: no new data was saved to " + output_file)

    print("Number of rows: " + str(no_rows))
    #print("Number of columns: " + str(no_col))
    print("Done!")


RESULT_FILE_1='/mc_staging/HC_sales/exf/1.0.0/raw/'  #--- Path in HDFS for storing as csv


#Running Reports one by one

run_report('mcloudmefslean',RESULT_FILE_1,True)

run_report('mcloudmefsphoenixmxg',RESULT_FILE_1,True)

run_report('mcloudmefstempo',RESULT_FILE_1,True)

run_report('mcloudmefsscalaseno',RESULT_FILE_1,True)

run_report('mcloudmefsscalafidk',RESULT_FILE_1,True)

run_report('mcloudmefsscalagrroczskhusrlt',RESULT_FILE_1,True)
