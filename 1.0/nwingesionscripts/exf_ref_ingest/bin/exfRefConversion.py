#! /opt/anaconda/3.7.1/lib/python3.7/site-packages

import pandas as pd
import glob
import os

print("========================================================================")
print("---------> Conversion for EXF sales data is triggred")
for x in glob.glob('/dfs/is/home/s114041/data2/EXF/*/*.xls*', recursive=True):
    print(x)
    filename = os.path.splitext(x)[0]
    print(filename)
    splitname = filename.split('/')
#    print(splitname)
#    print(splitname[7],splitname[8])
#    data_xls = pd.read_excel(x, 'Export', index_col=None)
    csvPath = "/dfs/is/home/s114041/data2/EXF_CODE/" + splitname[7]
    cmd='mkdir '+ csvPath
    print(cmd)
    stat=os.system(cmd)
    if stat==0:
        print("Created dir ",csvPath)
    else:
        print("Can not create dir ",csvPath)
    
    data_xls = pd.read_excel(x, 'Export', index_col=None)
    csvfilename = csvPath + "/" +splitname[8] + ".csv"
    print(csvfilename)
    data_xls.to_csv(csvfilename,encoding='utf-8', index=False, sep='|')
    print("-----------------------------------------------------------------------")
#    print(data_xls)

chcmd = "chmod -R 777 /dfs/is/home/s114041/data2/EXF_CODE/"
modstat=os.system(chcmd)
if modstat==0:
    print("CHMOD done")
else:
    print("CHMOD not done")

print("===========================================================================")


print("========================================================================")
print("---------> Conversion for EXF sales data is triggred")
for x in glob.glob('/dfs/is/home/s114041/data2/Reference_data_tables/*.xls*', recursive=True):
    print(x)
    filename = os.path.splitext(x)[0]
    print(filename)
    splitname = filename.split('/')
    data_xls = pd.read_excel(x, index_col=None)
    csvfilename = "/dfs/is/home/s114041/data2/Reference_tables_Code/" +splitname[7] + ".csv"
    print(csvfilename)
    data_xls.to_csv(csvfilename,encoding='utf-8', index=False, sep='|')
    print("-----------------------------------------------------------------------")
#    print(data_xls)

chcmd = "chmod -R 777 /dfs/is/home/s114041/data2/Reference_tables_Code/"
modstat=os.system(chcmd)
if modstat==0:
    print("CHMOD done")
else:
    print("CHMOD not done")

print("===========================================================================")

'''
print("Remove files and folders from dir")
rmcmd = "rm -r /dfs/is/home/s114041/data2/EXF_CODE/"
mkcmd = "mkdir /dfs/is/home/s114041/data2/EXF_CODE/"
os.system(rmcmd)
os.system(mkcmd)
'''
