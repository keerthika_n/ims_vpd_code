#!/bin/bash
#=============================================================#
#  Name : IMS_VPD Data to HDFS location                       #
#  Description : This script does following		      #
#	-Looks for the Excel files in the shared drive	      #
#	-Pushs data to HDFS locations specified		      #
#                                                             #
# Date:         20-September-2019                             #
# Author:       Sai Naveen Mandava                            #
#=============================================================#
#                  Modification Log                           #
#                  ----------------                           #
# V1.0 10-September-2019 Sai Naveen Mandava                   #
# Basic version is created to load data from Edge node to     #
# HDFS location                                               #
#-------------------------------------------------------------#
#                                                             #
#=============================================================#
#

logDir='/dfs/is/home/s114041/IMS_VPD/1.0/nwingesionscripts/exf_ref_ingest/bin/files'
logDate=`date +%Y-%m-%d`
dateTime=`date +%Y-%m-%d`
logFile=`echo "$logDir/$dateTime"".log"`
echo "`touch "$logFile"`"
yesterdayDate=$(date +%Y%m%d --date="yesterday")
#Todaydate=$YY$MM$DD



function Initialize(){
. /dfs/is/home/s114041/IMS_VPD/1.0/nwingesionscripts/exf_ref_ingest/bin/runkinit.sh
rc=$?
if [ $rc -eq 0 ]; then
kinit_initialize='TRUE'
echo "[$dateTime] [INFO] : Kinit executed successfully" >> $logFile
else
echo "[$dateTime] [ERROR] : Kinit Failed" >> $logFile
exit;
fi
}

function logger(){
	echo $msg >> $logFile
	error_msg="${error_msg}\n${msg}"
}


function error_routine() {
    ret_code=$1
    ret_message=$2

    if [ $ret_code != 0 ];
    then
        echo "Error in $ret_message" >> $logFile 2>&1
        exit 1
    fi
}



function Execution(){


echo "change mode"
#chmod -R o+rx '/shared/data/imsvpd' >> $logFile 

#echo "Id"
id >> $logFile

#Getting Current Year and Month

myyear=`date +'%Y'`
echo "Year = $myyear" >> $logFile

mymonth=$(date -d "$D" '+%b')
echo "Month = $mymonth" >> $logFile

#IMS Path test for year and creation if path does not exist
imsYearPath=/mc_staging/HC_sales/exf/1.0.0/raw/$myyear
echo $imsYearPath >> $logFile

hadoop fs -test -d ${imsYearPath};
if [ $? != 0 ]; then
        echo "Directory does not exists!" >> $logFile
	hadoop fs -mkdir ${imsYearPath}
else
        echo "Directory exists!" >> $logFile
fi

#IMS Path test for Month and creation if path does not exist
imsMonthPath=/mc_staging/HC_sales/exf/1.0.0/raw/$myyear/$mymonth
echo $imsMonthPath >> $logFile

hadoop fs -test -d ${imsMonthPath};
if [ $? == 0 ]; then
        echo "Directory exists!" >> $logFile
else
        echo "Directory not  exists!" >> $logFile
	hadoop fs -mkdir ${imsMonthPath}
fi

#Ingesion for IMS data
hadoop fs -put /dfs/is/home/s114041/data2/EXF_CODE/*  ${imsMonthPath} 

echo "Ingesion of EXF is done" >> $logFile

echo "Ingesion of Reference Data is started" >> $logFile

#Ingesion for Reference Data
#/dfs/is/home/s114041/IMS_VPD/1.0/nwingesionscripts/exf_ref_ingest/bin/referenceingest.sh>>$logFile 2>&1
return_code=$?
error_routine $return_code "Ingestion"

echo "Ingesion of Reference Data is Done" >> $logFile

echo  "[$dateTime] [INFO] : Ingesion done" >> $logFile
}




#Below helps in ingesion of Data from IMS, VPD, Reference_Data_Tables happen if KINIT is sucessfull

Initialize

if  [ ${kinit_initialize,,} == 'true' ]; then
    echo "Ingesion triggred" >> $logFile
    Execution
    status_rc=$?
else
    msg="[$dateTime] [INFO] : Kinit failed!! Exiting from script. No load to HDFS would be performed."
    echo $msg >> $logFile
    error_msg="${error_msg}\n${msg}"
fi

