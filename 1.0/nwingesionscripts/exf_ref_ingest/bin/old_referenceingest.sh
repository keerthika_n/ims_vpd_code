#!/bin/bash
#=============================================================#
#                                                             #
#            Reference Tables Ingesion Script                 #
#                                                             #
#=============================================================#




# Ingest files from network share to hdfs

#config=nwingestion
config=/dfs/is/home/s114041/ingesionscripts

echo "Ingestion from Network Share is initiated"

#hadoop fs -rm /mc_staging/HC_sales_demand/mapping_data/CIMD_MASTER_FILE/1.0.0/raw/CIMD_MASTER_FILE.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/CIMD_MASTER_FILE.xlsx /mc_staging/HC_sales_demand/mapping_data/CIMD_MASTER_FILE/1.0.0/raw/CIMD_MASTER_FILE.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/CORRECT_NEW_CHANNEL_PER_COUNTRY.xlsx /mc_staging/HC_sales_demand/mapping_data/CORRECT_NEW_CHANNEL_PER_COUNTRY/1.0.0/raw/CORRECT_NEW_CHANNEL_PER_COUNTRY.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/COUNTRY_MAPPING.xlsx /mc_staging/HC_sales_demand/mapping_data/COUNTRY_MAPPING/1.0.0/raw/COUNTRY_MAPPING.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/COUNTRY_MULTIPLE_IMS_SOURCES.xlsx /mc_staging/HC_sales_demand/mapping_data/COUNTRY_MULTIPLE_IMS_SOURCES/1.0.0/raw/COUNTRY_MULTIPLE_IMS_SOURCES.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/MPS_CHANNELS_CONSIDERED_PER_COUNTRY.xlsx /mc_staging/HC_sales_demand/mapping_data/MPS_CHANNELS_CONSIDERED_PER_COUNTRY/1.0.0/raw/MPS_CHANNELS_CONSIDERED_PER_COUNTRY.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/MPS_ENTITIES_FOR_ANALYSIS.xlsx /mc_staging/HC_sales_demand/mapping_data/MPS_ENTITIES_FOR_ANALYSIS/1.0.0/raw/MPS_ENTITIES_FOR_ANALYSIS.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/MPS_Number_of_SKUs.xlsx /mc_staging/HC_sales_demand/mapping_data/MPS_Number_of_SKUs/1.0.0/raw/MPS_Number_of_SKUs.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/PRODUCT_MAPPING.xlsx /mc_staging/HC_sales_demand/mapping_data/PRODUCT_MAPPING/1.0.0/raw/PRODUCT_MAPPING.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/WHOLESALERS_NOT_SHARING_IMS.xlsx /mc_staging/HC_sales_demand/mapping_data/WHOLESALERS_NOT_SHARING_IMS/1.0.0/raw/WHOLESALERS_NOT_SHARING_IMS.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/Z_Country_master_file.xlsx /mc_staging/HC_sales_demand/mapping_data/Z_Country_master_file/1.0.0/raw/Z_Country_master_file.xlsx

hadoop fs -put -f /shared/data/imsvpd/Reference_data_tables/Z_MAPPING_FOR_GLOBAL_PRODUCTS.xlsx /mc_staging/HC_sales_demand/mapping_data/Z_MAPPING_FOR_GLOBAL_PRODUCTS/1.0.0/raw/Z_MAPPING_FOR_GLOBAL_PRODUCTS.xlsx

echo "Ingestion done for Reference Tables to HDFS location"

