#! /opt/anaconda/3.7.1/lib/python3.7/site-packages

import pandas as pd
import glob
import os

print("========================================================================")
print("---------> Conversion for VPD sales data is triggred")

for x in glob.glob('/dfs/is/home/s114041/data2/IMS/*/VPD/*.xls*', recursive=True):
    print(x)
    filename = os.path.splitext(x)[0]
    print(filename)
    splitname = filename.split('/')
#    print(splitname)
    print(splitname[7],splitname[9])
#    data_xls = pd.read_excel(x, 'Sheet1', index_col=None)
    csvPath = "/dfs/is/home/s114041/data2/VPD_CODE/" + splitname[7]
    cmd='mkdir '+ csvPath
    print(cmd)
    stat=os.system(cmd)
    if stat==0:
        print("Created dir ",csvPath)
    else:
        print("Can not create dir ",csvPath)

    data_xls = pd.read_excel(x, 'Sheet1', index_col=None)
    csvfilename = csvPath + "/" +splitname[9] + ".csv"
    print(csvfilename)
    data_xls.to_csv(csvfilename,encoding='utf-8', index=False, sep='|')
    print("-----------------------------------------------------------------------")
#    print(data_xls)

chcmd = "chmod -R 777 /dfs/is/home/s114041/data2/VPD_CODE/"
modstat=os.system(chcmd)
if modstat==0:
    print("CHMOD done")
else:
    print("CHMOD not done")

print("===========================================================================")


print("========================================================================")
print("---------> Conversion for IMS sales data is triggred")

for x in glob.glob('/dfs/is/home/s114041/data2/IMS/*/IMS/*.xls*', recursive=True):
    print(x)
    filename = os.path.splitext(x)[0]
    print(filename)
    splitname = filename.split('/')
#    print(splitname)
    print(splitname[7],splitname[9])
#    data_xls = pd.read_excel(x, 'Sheet1', index_col=None)
    csvPath = "/dfs/is/home/s114041/data2/IMS_CODE/" + splitname[7]
    cmd='mkdir '+ csvPath
    print(cmd)
    stat=os.system(cmd)
    if stat==0:
        print("Created dir ",csvPath)
    else:
        print("Can not create dir ",csvPath)

    data_xls = pd.read_excel(x, index_col=None)
    mdf = data_xls.melt(id_vars=['Country','Brick','Product'],var_name='Yearmonth',value_name='Amount')
    mdf['Source']= "IMS"
    csvfilename = csvPath + "/" +splitname[9] + ".csv"
    print(csvfilename)
    mdf.to_csv(csvfilename,encoding='utf-8', index=False, sep='|')
    print("-----------------------------------------------------------------------")
#    print(data_xls)

chcmd = "chmod -R 777 /dfs/is/home/s114041/data2/IMS_CODE/"
modstat=os.system(chcmd)
if modstat==0:
    print("CHMOD done")
else:
    print("CHMOD not done")

print("===========================================================================")

