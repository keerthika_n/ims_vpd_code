#!/bin/bash
SESSION_ID=$(ps -p $$ --no-headers -o sid | tr -d '[:space:]')
export KRB5CCNAME=/dfs/is/home/s114041/IMS_VPD/1.0/nwingesionscripts/ims_vpd_ingest/bin/files/S114041_my_ticket_"$SESSION_ID"
kinit -kt /dfs/is/home/s114041/IMS_VPD/1.0/nwingesionscripts/ims_vpd_ingest/bin/S114041.headless.keytab S114041@EU.MERCKGROUP.COM -c $KRB5CCNAME
logdir=/dfs/is/home/s114041/IMS_VPD/1.0/nwingesionscripts/ims_vpd_ingest/bin/files
logfile=$logdir/log_session_$(date +%Y%m%d_%H%M%S)
klist

